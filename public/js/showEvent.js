$(document).ready(function(){

    $('.dayOfWeek').each(function (){
        console.log($(this).html());
        switch ($(this).html()) {
            case '0':
                $(this).html('日');
                break;
            case '1':
                $(this).html('月');
                break;
            case '2':
                $(this).html('火');
                break;
            case '3':
                $(this).html('水');
                break;
            case '4':
                $(this).html('木');
                break;
            case '5':
                $(this).html('金');
                break;
            case '6':
                $(this).html('土');
                break;
        }
    });

    $('[class^="editUser_"]').click(function (){
        // to prevent reload
        event.preventDefault();

        // added to different variables to prevent 'this' from changing values
        var rowThis = $(this).attr('class');
        var tableThis = $(this).attr('class');

        // add disabled and hide for all rows to reset
        $('.' + tableThis + '').closest('table').find('select').prop("disabled", true);
        $('.' + tableThis + '').closest('table').find('input').hide().prop("disabled", true);

        // removed disabled and show button for the row clicked
        $('.' + rowThis + '').closest('tr').find('select').prop("disabled", false);
        $('.' + rowThis + '').closest('tr').find('input').show().prop("disabled", false);

        event.preventDefault();
    });

    $('btn').on('click', function(){

        var a = $(this);
        var form = $('form');
        var action = a.attr('name');

        var url = window.location.href;

        form.attr('action', url + "/" + action + "/event" ).submit();
    });


});