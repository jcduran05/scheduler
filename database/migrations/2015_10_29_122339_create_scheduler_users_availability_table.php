<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerUsersAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedulerUsersAvailability', function(Blueprint $table)
        {
            $table->increments('userAvailability_id');
            $table->integer('scheduler_id')->references('id')->on('scheduler');
            $table->integer('date_id')->references('date_id')->on('schedulerDates');
            $table->integer('user_id')->references('user_id')->on('schedulerUsers');
            $table->string('available')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schedulerUsersAvailability');
    }
}
