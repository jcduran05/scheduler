@extends('app')

@section('content')
<div class="container">
	<div class="row">
	    <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
              <div class="panel-body">
                  <h3>Thank you. Your event has been created.</h3>
                  <p>Send this link to anyone you wish to invite.</p>
                  <p>For your record, the link has been sent to {{ $event_email }}.</p>
                  <a href="{{ $event_url }}">{{ $event_url }}</a>
              </div>
          </div>
	    </div>
	</div>
</div>
@endsection
