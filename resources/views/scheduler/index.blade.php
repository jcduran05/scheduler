@extends('app')

@section('content')

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        {{--<div class="panel-heading">--}}
          {{--<h3 class="panel-title">Create an event</h3>--}}
        {{--</div>--}}
        <div class="panel-body">
           <h3 style="margin-top: 0px;">Create an event</h3>
          {{--FORM START--}}
          {!! Form::open(['route' => array('scheduler.store')]) !!}
          <div class="form-group @if ($errors->has('title')) has-error @endif">
            {!! Form::label('title', 'Title:', ['class' => '']) !!}
            {!! Form::text('title', null, ['class' => 'form-control input-sm']) !!}
            @if ($errors->has('title'))<p class="help-block">{!!  $errors->first('title') !!}</p>@endif
          </div>
          <div class="form-group @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email', 'Email:', ['class' => '']) !!}
            {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!}
            @if ($errors->has('email'))<p class="help-block">{!!  $errors->first('email') !!}</p>@endif
          </div>
          <div class="form-group">
            {!! Form::label('location', 'Location:', ['class' => '']) !!}
            {!! Form::text('location', null, ['class' => 'form-control input-sm', 'id' => 'location', 'placeholder' => '']) !!}
          </div>
          <div class="row">
            <div class="col-md-6 @if ($errors->has('date')) has-error @endif">
              {!! Form::label('dates', 'Dates:', ['class' => '']) !!}
              @if ($errors->has('date'))<p class="help-block">{!!  $errors->first('date') !!}</p>@endif
              <div class="datesPicked list" style="width: 100%; text-align: center;">
                @if (Input::old('dates') )
                @foreach( old('dates') as $key => $val )
                <div class="item">{!! $val !!}&nbsp;
                  <button type="button" class="deleteDate btn btn-trash btn-danger">
                    <i class="fa fa-trash"></i></button>
                    <input type="hidden" name="date" value="true">
                    <input type="hidden" class="dates_arr" name="dates[{!! $key !!}]" value="{!! $val !!}" >
                  </div>
                  @endforeach
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div id="datepicker"></div>
              </div>
            </div>
            {{--<div class="form-group">--}}
              {{--{!! Form::label('password', '(Optional) Password:', ['class' => 'inline']) !!}--}}
              {{--{!! Form::text('password', null, ['class' => 'form-control input-sm']) !!}--}}
            {{--</div>--}}
            <div class="form-group text-right">
              {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
            </div>
            {!! Form::close() !!}
            {{--FORM END--}}
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyD9it-XVaNwLTUVcMzPwQsAzvnxZBmprqY"></script>

  <script>
    String.prototype.replaceAll = function(search, replace) {
      if (replace === undefined) {
        return this.toString();
      }
      return this.split(search).join(replace);
    }


    $(document).ready(function(){
      var dateNum = 1;

      $('.dates_arr').each(function(){
        dateNum++;
      });

      $(function(){

        // Initialize Datepicker
        $("#datepicker").datepicker({
          dateFormat: 'yy/mm/dd',
          showMonthAfterYear: true,
          minDate: new Date(),
          maxDate: '+12m',
          hideIfNoPrevNext: true,
          // 日付が選択された時、日付をテキストフィールドへセット
          onSelect: function(dateText, inst) {
            var nowText = $("#date").val();
            var formatted = dateText.replaceAll("/", "");
            var hiddenName = formatted.substr(0, 8);


            if(nowText === ""){
              //$("#date").val(dateText);
              $(".datesPicked").append(
                '<div class="item">' + dateText +
                '&nbsp;&nbsp;<button type="button" class="deleteDate btn btn-trash btn-danger">' +
                '<i class="fa fa-trash"></i></button>' +
                '<input type="hidden" name="date" value="true">' +
                '<input type="hidden" class="dates_arr" name="' + 'dates[' + dateNum + ']" value="' + dateText + '" ></div>'
                );
            } else{
            //$("#date").val(nowText+"\n"+dateText);
            $(".datesPicked").append(
              '<div class="item">' + dateText +
              '&nbsp;&nbsp;<button type="button" class="deleteDate btn btn-trash btn-danger">' +
              '<i class="fa fa-trash"></i></button>' +
              '<input type="hidden" name="date" value="true">' +
              '<input type="hidden" class="dates_arr" name="' + 'dates[' + dateNum + ']" value="' + dateText + '" ></div>'
              );
          }

          dateNum++;
        }
      });

        // Remove list item
        $(document).on('click', '.deleteDate', function() {
          $(this).parent().remove();
        });

        // Google Places API
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);

      });
});
</script>

@endsection
