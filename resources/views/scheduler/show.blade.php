@extends('app')

@section('content')
<link href="/css/showEvent.css" rel="stylesheet">
<!-- Custom CSS -->
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {{--FORM START--}}
            {!! Form::open(['route' => array('scheduler.addUser', $url)]) !!}
            <div class="panel panel-default">
              {{--<div class="panel-heading">--}}
                {{--<h2 class="panel-title">Event title&nbsp;:&nbsp;{{ $event_title }}</h2>--}}
            {{--</div>--}}
            <div class="panel-body">
                <h3 style="margin-top: 0px;">Event: {{ $event_title }}</h3>
                <h5 style="margin-top: 0px;">Location: {{ $event_location }}</h5>
                <div class="input-group @if ($errors->has('user')) has-error @endif">
                    {!! Form::text('user', null, ['class' => 'form-control input-sm', 'placeholder' => 'Input user']) !!}
                    <span class="input-group-btn">
                        {!! Form::submit('Add', ['class' => 'btn btn-info btn-sm', 'id' => 'add-user']) !!}
                    </span>
                </div>
                 @if ($errors->has('user'))<p class="help-block" style="color: #DA4453">{!!  $errors->first('user') !!}</p>@endif
            {!! Form::close() !!}
            {{--FORM END--}}

                {!! Form::open(array('url' => 'scheduler/' . $url . '/update')) !!}
                <div class="" style="margin-top: 25px;">
                    @if ($event_users != null)
                    <table class="table table-condensed table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="clear-left-row-header"></th>
                                @foreach ($event_dates as $dates)
                                <th class="text-center aligned">{{ date("Y/m/d",strtotime($dates->date)) }}
                                {{--&nbsp;(<span class="dayOfWeek">{{ date("w",strtotime($dates->date)) }}</span>)--}}
                                </th>
                                @endforeach
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($event_users as $users)
                            <tr class="">
                                <td class="text-center" style="width: 120px;"><a href="#" id="" class="editUser_{{ $users->user_id }}" prevent-default>{{ $users->user }}</a></td>
                                @foreach ($user_availability as $dates)

                                @if ($users->user_id == $dates->user_id)
                                {{--<td class="text-center">{!! Form::checkbox($users->user . "_" . date("Ymd",strtotime($dates->date)), '0', false, array('disabled')); !!}</td>--}}
                                <td class="text-center center aligned">
                                <span class="">
                                    <input type="radio" class="circle-x-radio unavailable" name="{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" id="unavailable_{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" value="0" @if($dates->available == '0') checked @endif/>
                                    <label for="unavailable_{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" class="unavailable-label" id=""></label>
                                </span>
                                <span class="">
                                    <input type="radio" class="circle-checked-radio" name="{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" id="available_{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" value="1" @if($dates->available == '1') checked @endif/>
                                    <label for="available_{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" id=""></label>
                                </span>
                                <span class="">
                                    <input type="radio" class="circle-minus-radio" name="{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" id="maybe_{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" value="2" @if($dates->available == '2') checked @endif/>
                                    <label for="maybe_{{ $users->user_id . "_" . date("Ymd",strtotime($dates->date)) . "_" . $dates->date_id }}" id=""></label>
                                </span>
                                </td>
                                @endif
                                @endforeach
                                <td class="text-center" style="width: 130px;">
                                    {{--{!! Form::hidden('user', $users->user, 'disabled'); !!}--}}
                                    <input type="hidden" name="user_id" value="{{$users->user_id}}" disabled />
                                    {!! Form::submit('Save', ['type' => 'submit', 'class' => 'usrDataSave btn btn-sm btn-info btn-sv-user', 'style' => 'display:none', 'name' => 'update']) !!}
                                    {!! Form::submit('Delete', ['type' => 'submit', 'class' => 'usrDataDelete btn btn-sm btn-danger btn-sv-user', 'style' => 'display:none', 'name' => 'delete']) !!}
                                </td>
                            </tr>
                            @endforeach

                            <tr>
                                <td class="text-center">Total:</td>
                                 @foreach ($availability_total as $availability)
                                    <td class="text-center" style="width: 130px;">
                                        <span>{{$availability}}</span>
                                    </td>
                                 @endforeach
                                 <td class="text-center"></td>
                            </tr>
                        </tbody>
                    </table>
                    @endif
            </div>
            @if ($event_users != null)
            <div class="row">
                <div class="col-md-12"> * Click on user to see options</div>
            </div>
            @endif
        </div>
    </div>
</div>
</div>
{!! Form::close() !!}
{{--FORM END--}}

{!! Form::open(array('url' => 'scheduler/' . $url . '/comment')) !!}
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            {{--<div class="panel-heading">--}}
                {{--<h3 class="panel-title">Comments</h3>--}}
            {{--</div>--}}
            <div class="panel-body">
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                    {{--{!! Form::label('name', 'Name:') !!}--}}
                    {!! Form::text('name', null, ['class' => 'form-control input-sm', 'placeholder' => 'Name']) !!}
                    @if ($errors->has('name'))<p class="help-block">{!!  $errors->first('name') !!}</p>@endif
                </div>
                <div class="form-group @if ($errors->has('comment')) has-error @endif">
                    {{--{!! Form::label('comment', 'Comment:') !!}--}}
                    {!! Form::textarea('comment', null, ['class' => 'form-control input-sm', 'style' => 'min-height: 6em; height: 6em;', 'placeholder' => 'Comment']) !!}
                    @if ($errors->has('comment'))<p class="help-block">{!!  $errors->first('comment') !!}</p>@endif
                </div>
                <div class="form-group text-right">
                    <div class="form-group">
                        {!! Form::submit('Post Comment', ['class' => 'btn btn-sm btn-info', 'id' => 'add-user']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                     @if ($comments != null)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Comment</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($comments as $coment)
                                <tr class="">
                                    <td class="" style="width: 120px;">{{$coment->name}}</td>
                                    <td class="">{{$coment->comment}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                     @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
{{--FORM END--}}
</div>

<script src="/js/showEvent.js"></script>
@endsection
