<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Scheduler</title>

  <!-- Bootstrap CSS -->
  <link href="/css/app.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="/css/custom.css" rel="stylesheet">
  <link href="/css/pickadate/default.css" rel="stylesheet">
  <link href="/css/pickadate/default.date.css" rel="stylesheet">
  <link href="/css/pickadate/default.time.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

  <!-- Fonts -->
  <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->

   <!-- Scripts -->
   <script src="/js/app.js"></script>
   <script src="/js/pickadate/picker.js"></script>
   <script src="/js/pickadate/picker.date.js"></script>
   <script src="/js/pickadate/picker.time.js"></script>
 </head>
 <body class="event-page" style="background-color: #f2f2f2">
  <!-- Navigation -->
    <nav class="navbar navbar-white navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="/"><i class="fa fa-calendar-check-o"></i>&nbsp;EventNote</a>
        </div>
      </div>
    </nav>
  @yield('content')

</body>
</html>
