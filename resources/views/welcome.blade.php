@extends('app')

@section('content')

<div class="container">
    <div class="col-md-8 col-md-offset-2 text-center">
        <h2>Simplify scheduling</h2>
        <h4>A simple scheduling tool to find a date to meet up.</h4>
        <a href="{{ URL::to('scheduler/create') }}" class="btn btn-info">Schedule an event</a>
    </div>
</div>
@endsection