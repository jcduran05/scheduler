<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	//
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schedulerComments';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'comment_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'comment'];


    public function comment()
    {
        return $this->belongsTo('App\Scheduler');
    }
}
