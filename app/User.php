<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'schedulerUsers';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['user_id'];

    public function scheduler_user()
    {
        return $this->belongsTo('App\Scheduler');
    }

    public function users_availability()
    {
        return $this->hasMany('App\UserAvailability');
    }

}
