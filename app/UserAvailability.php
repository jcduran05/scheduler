<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAvailability extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schedulerUsersAvailability';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'userAvailability_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['available'];


    public function users()
    {
        return $this->belongsTo('App\User');
    }

}
