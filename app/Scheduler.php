<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'scheduler';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'password', 'location', 'email'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['url', 'password'];

    /**
     * Create unique url for the event
     *
     */
    public static function  get_unique_url() {

        // set a random number
        $number = rand(10000, 9999999);

        // character list for generating a random string
        $charlist = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $decimal = intval($number);

        //get the list of valid characters
        $charlist = substr($charlist, 0, 62);

        $converted = '';

        while($number > 0) {
            $converted = $charlist{($number % 62)} . $converted;
            $number = floor($number / 62);
        }

        if( static::whereUrl($converted)->first() ) {
            static::get_unique_url();
        }

        return $converted;
    }

    /**
     * Establish relationship with users
     *
     */
    public function users()
    {
        return $this->hasMany('App\User');
    }

    /**
     * Establish relationship with dates
     *
     */
    public function dates()
    {
        return $this->hasMany('App\Date', 'scheduler_id');
    }

    /**
     * Establish relationship with comments
     *
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

}
