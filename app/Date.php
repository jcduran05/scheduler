<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schedulerDates';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'date_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['date'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['scheduler_id'];

    public function scheduler()
    {
        return $this->belongsTo('App\Scheduler');
    }

}
