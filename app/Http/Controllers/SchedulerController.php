<?php namespace App\Http\Controllers;

use App\Scheduler;
use App\Date;
use App\User;
use App\Comment;
use App\UserAvailability;
use Illuminate\Http\Request;
use App\Http\Requests\SchedulerRequest;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UserAvailabilityRequest;
use App\Http\Requests\CommentRequest;
use App\Http\Controllers;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Domain\Repository\CreateSchedulerInterface;
use Domain\Repository\EditSchedulerInterface;

class SchedulerController extends Controller {

    public function __construct(CreateSchedulerInterface $newEvent, EditSchedulerInterface $edit)
    {
        $this->newEvent = $newEvent;
        $this->edit = $edit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return redirect('scheduler/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('scheduler.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SchedulerRequest $request)
    {
        // Initialize new Event and Request data
        $event = $this->newEvent->initNewEvent($request);
        //$event->url = $this->newEvent->create_unique_url($event);
        $event->url = Scheduler::get_unique_url();

        if ($request->input('add_password') == 'true') {
            $event->password = Hash::make($event['password']);
        }

        // Save the event
        $this->newEvent->storeEvent($event);

        // Organize inputted date data
        $datesArr = $this->newEvent->getDates($request);

        // Save each date with data from $event
        foreach($datesArr as $record) {
            $formattedDate = Carbon::parse($record);
            $recordArray = ['date' => $formattedDate];
            $date = new Date($recordArray);
            $date->scheduler()->associate($event);

            $date->save();
        }

        // Schedule an email to be sent
        $this->newEvent->sendEventEmail($event);

        return redirect('scheduler/complete')->with('eventData', ['url' => $event->url, 'email' => $event->email]);
    }

    /**
     * Event created. Display the URL to the user.
     *
     * @return Response
     */
    public function complete()
    {
        $base_url = URL::to('/'). "/scheduler/";
        $data = Session::get('eventData');
        $event_url = $base_url . $data['url'];
        $event_email = $data['email'];

        return view('scheduler.complete', compact('event_url', 'event_email'));
    }

    /**
     * Send an e-mail reminder to the user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function sendEmailReminder(Request $request, $id)
    {
        $user = User::findOrFail($id);

        Mail::send('emails.reminder', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Your Reminder!');
        });
    }

    /**
     * Display the specified event.
     *
     * @param  string $url
     * @return Object
     */
    public function show($url)
    {
        // Basic event data
//        $event_users        = $this->edit->eventData($url);
//        $event_comments     = $this->edit->eventData($url);
//        $event              = $this->edit->eventBasicData($url);

        // event_data to hold most of the data. All except
        // comment will filter the data from event_data
        $event_data         = $this->edit->eventData($url);

        if(empty($event_data)) {
            return redirect('scheduler/');
        }

        $event_title        = $this->edit->eventTitle($event_data);
        $event_location     = $event_data[0]->location;
        $event_dates        = $this->edit->eventDates($event_data);

        // Event users and their availability
        $event_users        = $this->edit->eventUsers($event_data);
        $user_availability  = $this->edit->userAvailability($event_data);
        $availability_total = [];

        foreach ($user_availability as $user) {
            $availability_total[$user->date_id] = 0;
        }

        foreach ($user_availability as $user) {
            if($user->available == 1) {
                $availability_total[$user->date_id] += 1;
            } else if ($user->available == 2) {
                $availability_total[$user->date_id] += 0.5;
            }
        }

        // Event comments
        $comments = $this->edit->event_comments($event_data);


        return view('scheduler.show', compact('url', 'event', 'event_data', 'event_title', 'event_location',
                    'event_dates', 'event_users', 'user_availability', 'availability_total', 'comments')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($url, Request $request)
    {
        // Get event data based on the event url
        $event = $this->edit->findEvent($url);

        $users = new User($request->all());

        $users->scheduler_user()->associate($event);
        $users->save();

        return view('scheduler.show', compact('event'));
    }

    public function addUser($url, AddUserRequest $request)
    {
        // Get event data based on the event url
        $event = $this->edit->findEvent($url);

        // Add a new user to the event
        $user = $this->edit->addUser($request, $event);

        // Load user data with event and dates information
        $user_data = $this->edit->findEventUser($user);

        // Create user data for all event dates
        $user_availability = $this->edit->addUserAvailability($user_data);

        return redirect('scheduler/'. $url);
    }

    public function saveUserData($url, Request $request){
        $user_id = $request->input('user_id');

        // Get user id and name based on an id
        $user_data = $this->edit->getUserData($url, $user_id);

        // Get and organizes dates in an array
        $dates = $this->edit->getDates($request);

        // Updates the users availability
        $this->edit->updateUserAvailability($dates, $user_data);

        return $url;
    }

    public function deleteUserData($url, Request $request){
        $user_id = $request->input('user_id');

        // Get user id and name based on an id
        $user_data = $this->edit->getUserData($url, $user_id);

        // Get and organizes dates in an array
        $dates = $this->edit->getDates($request);

        // delete user availability. Need to find way for it to cascade
        $this->edit->deleteUser($dates, $user_data);

        return $url;
    }

    public function eventAction($url, Request $request)
    {
        //check which submit was clicked on
        if($request->input('update')) {
            $this->saveUserData($url, $request); //if login then use this method
        } elseif($request->input('delete')) {
            $this->deleteUserData($url, $request); //if register then use this method
        }

        return redirect('scheduler/'. $url);
    }

    public function comment($url, CommentRequest $request)
    {
        $event = $this->edit->eventBasicData($url);

        $comment = new Comment($request->all());
        $comment->scheduler_id = $event[0]->id;
        $comment->save();

        return redirect('scheduler/'. $url);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
