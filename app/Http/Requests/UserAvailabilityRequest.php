<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserAvailabilityRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $check = [
            'scheduler_id' => 'required',
            'date_id'  => 'required',
            'user_id'  => 'required',
        ];

        return $check;
    }

}
