<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// scheduler (create event) controller
App::bind('Domain\Repository\CreateSchedulerInterface', 'Domain\Repository\CreateSchedulerRepository');
App::bind('Domain\Repository\EditSchedulerInterface', 'Domain\Repository\EditSchedulerRepository');

Route::post('scheduler/{url}/add', ['as' => 'scheduler.addUser', 'uses' => 'SchedulerController@addUser']);
Route::post('scheduler/{url}/comment', array('uses' => 'SchedulerController@comment'));
Route::post('scheduler/{url}/update', array('uses' => 'SchedulerController@eventAction'));

Route::get('scheduler/complete', 'SchedulerController@complete');
Route::resource('scheduler', 'SchedulerController');

