<?php namespace Domain\Repository;

use App\SocialEvent;
use Domain\Service\Cache\EventCache;
use Domain\Repository\Event\CacheDecorator;
use Domain\Repository\Event\EventRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        /**
         * User Repository
         *
         * @return Acme\Repository\Event\EventRepository
         */
        $this->app->bind('Scheduler\Repository\Event\EventInterface', function($app)
        {
            $event = new EventRepository (new SocialEvent);

            return new CacheDecorator($event, new EventCache($app['cache'], 'event')
            );
        });
    }

}