<?php namespace Domain\Repository;

use App\Http\Requests\SchedulerRequest;
use App\Http\Requests\UserAvailabilityRequest;
use Illuminate\Support\Facades\Mail;

interface CreateSchedulerInterface {

    public function initNewEvent(SchedulerRequest $request);

    public function storeEvent($event);

    public function getDates(SchedulerRequest $request);

    public function check_password_field($event);

    public function create_unique_url($event);

    public function sendEventEmail($event);
}