<?php namespace Domain\Repository;

use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UserAvailabilityRequest;

interface EditSchedulerInterface {

    public function eventBasicData($url);

    public function eventData($url);

    public function eventTitle($event);

    public function eventDates($event);

    public function getDates(Request $request);

    public function eventUsers($event);

    public function userAvailability($event);

    public function addUser(AddUserRequest $request, $event);

    public function addUserAvailability($user_data);

    public function updateUserAvailability($dates, $user_data);

    public function deleteUser($dates, $user_data);

    public function event_comments($event);

    public function findEvent($url);

    public function findEventAllData($url);

    public function findEventUser($user);

    public function getUserData($url, $id);
}