<?php namespace Domain\Repository;

use Illuminate\Support\Facades\DB;
use App\User;
use App\UserAvailability;
use Illuminate\Http\Request;
use App\Http\Requests\SchedulerRequest;
use App\Http\Requests\AddUserRequest;
use App\Http\Requests\UserAvailabilityRequest;

class EditSchedulerRepository implements EditSchedulerInterface {

    public function eventBasicData($url) {
        $event = DB::table('scheduler')
            ->distinct()
            ->select('scheduler.id', 'scheduler.url', 'scheduler.title', 'schedulerUsers.user')
            ->leftJoin('schedulerUsers', 'scheduler.id', '=', 'schedulerUsers.scheduler_id')
            ->where('scheduler.url', $url)->get();


        return $event;
    }

    public function eventData($url) {
        $event = DB::table('scheduler')
            ->select('scheduler.id', 'scheduler.url', 'scheduler.title', 'scheduler.location', 'schedulerUsers.user',
                    'schedulerUsers.user_id', 'schedulerUsers.user',
                    'schedulerUsersAvailability.available', 'schedulerUsersAvailability.user_id as user_availability_id',
                    'schedulerDates.date', 'schedulerDates.date_id')
            ->leftJoin('schedulerUsers', 'scheduler.id', '=', 'schedulerUsers.scheduler_id')
            ->leftJoin('schedulerUsersAvailability', function($join) {
                    $join->on('scheduler.id', '=', 'schedulerUsersAvailability.scheduler_id')
                     ->on('schedulerUsers.user_id', '=', 'schedulerUsersAvailability.user_id');
            })
            ->leftJoin('schedulerDates', function($join) {
                    $join->on('scheduler.id', '=', 'schedulerDates.scheduler_id')
                    ->on('schedulerDates.date_id', '=', 'schedulerUsersAvailability.date_id');
            })
//            ->leftJoin('schedulerComments', 'scheduler.id', '=', 'schedulerComments.scheduler_id')
            ->where('scheduler.url', $url)
            ->orderBy('schedulerUsers.user_id')
            ->orderBy('schedulerDates.date_id')
            ->orderBy('schedulerUsers.user')
            ->get();

        return $event;
    }

    public function eventTitle($event) {
        $title = $event[0]->title;

        return $title;
    }

    public function eventDates($event){
        $dates = DB::table('schedulerDates')
            ->select('schedulerDates.date', 'schedulerDates.date_id')
            ->where('schedulerDates.scheduler_id', $event[0]->id)->get();

//        $event_dates = [];
//        foreach ($event as $c) {
//            $event_dates[$c->date_id] = $c; // Get unique country by code.
//        }

        return $dates;
    }

    /**
     * Get date data and organize in an array
     *
     * @param  Request $request
     * @return Object
     */
    public function getDates(Request $request)
    {
        // Regex pattern. considering making it stronger
        $pattern = '/[0-9]{8}_[0-9]{1,10}/';
        $dates = array();

        foreach($request->all() as $key => $item){
            preg_match($pattern, $key, $matches);
            //check that there is a match
            //so that 0 won't be offset
            if(!empty($matches)){
                $values = array(
                    'date'  => $matches[0],
                    'value' => $item
                );
                array_push($dates, $values);
            }
        }

        return $dates;
    }

    public function eventUsers($event){
//        $users = DB::table('schedulerUsers')
//            ->select('schedulerUsers.user_id', 'schedulerUsers.user')
//            ->where('schedulerUsers.scheduler_id', $event[0]->id)->get();

        $user_data = [];

        foreach ($event as $e) {
            if($e->user_id != '') {
                $user_data[$e->user_id] = $e; // Get unique user id.
            }
        }

        return $user_data;
    }

    public function userAvailability($event)
    {
//        $user_availability = DB::table('schedulerUsersAvailability')
//            ->select('schedulerUsersAvailability.user_id','schedulerUsersAvailability.available', 'schedulerDates.date', 'schedulerDates.date_id')
//            ->leftJoin('schedulerDates', 'schedulerUsersAvailability.date_id', '=', 'schedulerDates.date_id')
//            ->where('schedulerUsersAvailability.scheduler_id', $event[0]->id)->get();

        $user_availability_data = [];

        foreach ($event as $e) {
            if($e->user_id != '') {
                $user_availability_data[$e->user_id."_".$e->date_id] = $e; // Get unique user id.
            }
        }


//        dd($event);
        return $user_availability_data;
    }

    public function addUser(AddUserRequest $request, $event)
    {
        // Add a new user to the event
        $user = new User($request->all());

        $user->scheduler_id = $event->id;
        $user->save();

        return $user;
    }

    public function addUserAvailability($user_data)
    {
        // Save user availability
        foreach ($user_data as $user) {
            $user_availability = new UserAvailability();
            $user_availability->scheduler_id = $user->scheduler_id;
            $user_availability->date_id = $user->date_id;
            $user_availability->user_id = $user->user_id;

            $user_availability->save();
        }

        return $user_availability;
    }

    public function updateUserAvailability($dates, $user_data)
    {
        foreach($dates as $date) {
            $ids = explode("_", $date['date']);
            UserAvailability::where('date_id', '=', $ids[1])
                ->where('user_id', '=', $user_data[0]->user_id)
                ->update(array('available' => $date['value']));
        }
    }

    public function deleteUser($dates, $user_data)
    {
        foreach($dates as $date) {
            $ids = explode("_", $date['date']);
            //dd(UserAvailability::where('date_id', '=', $ids[1])->where('user_id', '=', $user_id));
            UserAvailability::where('date_id', '=', $ids[1])
                ->where('user_id', '=', $user_data[0]->user_id)
                ->delete();
        }

        // deletion should cascade. find way to delete user availability
        // and user data at the same time
        User::where('user_id', '=', $user_data[0]->user_id)->delete();
    }

    public function event_comments($event)
    {
        $event_comments = DB::table('schedulerComments')
            ->select('schedulerComments.name','schedulerComments.comment')
            ->where('schedulerComments.scheduler_id', $event[0]->id)->get();

        return $event_comments;
    }

    public function findEvent($url)
    {
        $find_event = DB::table('scheduler')
            ->leftJoin('schedulerDates', 'schedulerDates.date_id', '=', 'scheduler.id')
            ->where('scheduler.url', $url)->first();

        return $find_event;
    }

    public function findEventAllData($url){

        $all_event_data = DB::table('scheduler')
            ->distinct()
            ->select('scheduler.id', 'scheduler.url', 'scheduler.title', 'schedulerDates.date',
                'schedulerUsers.user_id', 'schedulerUsers.user', 'schedulerUsersUsersAvailability.available')
            ->leftJoin('schedulerDates', 'scheduler.id', '=', 'schedulerDates.scheduler_id')
            ->leftJoin('schedulerUsers', 'schedulerDates.scheduler_id', '=', 'schedulerUsers.scheduler_id')
            ->leftJoin('schedulerUsersUsersAvailability', 'schedulerUsers.user_id', '=', 'schedulerUsersUsersAvailability.user_id')
            ->where('scheduler.url', $url)->get();

        return $all_event_data;
    }

    public function findEventUser($user)
    {
        $id = DB::table('scheduler')
            ->leftJoin('schedulerDates', 'schedulerDates.scheduler_id', '=', 'scheduler.id')
            ->leftJoin('schedulerUsers', 'schedulerUsers.scheduler_id', '=', 'scheduler.id')
            ->where('schedulerUsers.user_id', $user->user_id)->get();

        return $id;
    }

    public function getUserData($url, $id)
    {
        $user_data = DB::table('scheduler')
            ->select('schedulerUsers.user_id', 'schedulerUsers.user')
            ->leftJoin('schedulerUsers', 'scheduler.id', '=', 'schedulerUsers.scheduler_id')
            ->where('scheduler.url', $url)
            ->where('schedulerUsers.user_id', $id)->get();

        return $user_data;
    }

}

