<?php namespace Domain\Repository\Event;

interface EventInterface {

    public function all();

    public function find($id);

    public function findEvent($id);

}