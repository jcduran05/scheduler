<?php namespace Domain\Repository\Event;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EventRepository implements EventInterface {

    protected $event;

    // Class expects an Eloquent model
    public function __construct(Model $event)
    {
        $this->event = $event;
    }

    public function all(){
        return $this->event->all();
    }

    public function find($id){
        return $this->event->with('event_details')->find($id);
    }

    public function findEvent($id){

        $event = DB::table('events')
            ->leftJoin('event_details', 'events.id', '=', 'event_details.event_id')
            ->where('events.id', $id)->get();

        return $event;
    }
}