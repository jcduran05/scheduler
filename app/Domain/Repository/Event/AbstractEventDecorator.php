<?php namespace Domain\Repository\Event;

abstract class AbstractEventDecorator implements EventInterface {

    /**
     * @var UserRepository
     */
    protected $event;

    /**
     * Construct
     *
     * @param UserRepository $user
     */
    public function __construct(EventInterface $event)
    {
        $this->event = $event;
    }

    public function allEvents(){
        return $this->event->allEvents();
    }

    public function findEvent($id)
    {
        return $this->event->findEvent($id);
    }

    /**
     * All
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->event->all();
    }

    /**
     * Find
     *
     * @param int $id
     * @return Illuminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        return $this->event->find($id);
    }

    /**
     * Create
     *
     * @param array $data
     * @return boolean
     */
    public function create(array $data)
    {
        return $this->event->create($data);
    }

    /**
     * Update
     *
     * @param array $data
     * @return boolean
     */
    public function update(array $data)
    {
        return $this->event->update($data);
    }

    /**
     * Delete
     *
     * @param int $id
     * @return boolean
     */
    public function delete($id)
    {
        return $this->event->delete($id);
    }

}