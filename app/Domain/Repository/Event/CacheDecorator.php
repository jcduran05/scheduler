<?php namespace Domain\Repository\Event;

use Domain\Service\Cache\EventCacheInterface;

class CacheDecorator extends AbstractEventDecorator {

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * Construct
     *
     * @param EventInterface $user
     * @param CacheInterface $cache
     */
    public function __construct(EventRepository $event, EventCacheInterface $cache)
    {
        parent::__construct($event);
        $this->cache = $cache;
    }

    public function allEvents()
    {
        $key = md5('all_events');

        if($this->cache->has($key))
        {
            return $this->cache->get($key);
        }

        $events = $this->event->all();

        $this->cache->put($key, $events);

        return $events;
    }

    public function findEvent($id)
    {
        $key = md5('event_id'.$id);

        if($this->cache->has($key))
        {
            return $this->cache->get($key);
        }

        $events = $this->event->findEvent($id);

        $this->cache->put($key, $events);

        return $events;
    }

    /**
     * All
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        $key = md5('all');

        if($this->cache->has($key))
        {
            return $this->cache->get($key);
        }

        $users = $this->event->all();

        $this->cache->put($key, $users, 1);

        return $users;
    }

    /**
     * Find
     *
     * @param int $id
     * @return Illuminate\Database\Eloquent\Model
     */
    public function find($id)
    {
        $key = md5('id.'.$id);

        if($this->cache->has($key))
        {
            return $this->cache->get($key);
        }

        $user = $this->event->find($id);

        $this->cache->put($key, $user);

        return $user;
    }
}