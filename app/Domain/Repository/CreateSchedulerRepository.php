<?php namespace Domain\Repository;

use App\Scheduler;
use App\Http\Requests\SchedulerRequest;
use App\Http\Requests\UserAvailabilityRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class CreateSchedulerRepository implements CreateSchedulerInterface {

    /**
     * Initialize request and return request data
     *
     * @param  SchedulerRequest $request
     * @return Object
     */
    public function initNewEvent(SchedulerRequest $request)
    {
        $event = new Scheduler($request->all());

        return $event;
    }

    /**
     * Save the event
     *
     * @param  object $event
     * @return boolean
     */
    public function storeEvent($event)
    {
        $event_date = $event->save();

        return $event_date;
    }

    /**
     * Save the event
     *
     * @param  object $event
     * @return boolean
     */
    public function sendEventEmail($event)
    {
        $base_url = URL::to('/'). "/scheduler/";
        $email = $event->url;
        $event_url = $base_url . $email;

        Mail::send('emails.complete', ['event' => $event, 'eventUrl' => $event_url], function ($m) use ($event) {
            $m->to($event->email)->subject('Event URL');
        });

        return $event;
    }

    /**
     * Get date data and organize in an array
     *
     * @param SchedulerRequest $request
     * @return Object
     */
    public function getDates(SchedulerRequest $request)
    {
        // Regex pattern
//        $pattern = '/^date_[0-9]{8}/';
        $datesArr = array();

//        foreach($request->input('dates') as $key => $item){
//            dd($item);
//
//            // Checking that there is a match
//            // So that 0 won't be offset
//            if(!empty($matches)){
//                $values = array(
//                    'date'  => $matches[0],
//                    'value' => substr($item, 0, 10)
//                );
//
//                $datesArr[] = $values;
//            }
//        }

        foreach ($request->input('dates') as $key => $item) {
                $datesArr[] = $item;
        }

        return $datesArr;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $event
     * @return boolean
     */
    public function check_password_field($event)
    {
        if (Input::get('add_password') == 'true') {
            $event->password = Hash::make($event['password']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  object $event
     * @return String
     */
    public function create_unique_url($event) {

        // set a random number
        $number = rand(10000, 9999999);

        // character list for generating a random string
        $charlist = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        $decimal = intval($number);

        //get the list of valid characters
        $charlist = substr($charlist, 0, 62);

        $converted = '';

        while($number > 0) {
            $converted = $charlist{($number % 62)} . $converted;
            $number = floor($number / 62);
        }

        if( static::whereUrl($converted)->first() ) {
            static::create_unique_url($event);
        }

        return $converted;
    }
}